package com.example.bobsburgers.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.activity.viewModels
import androidx.core.view.forEach
import androidx.recyclerview.widget.GridLayoutManager
import com.example.bobsburgers.databinding.ActivityMainBinding
import com.example.bobsdata.model.CharacterRepo
import com.example.bobsburgers.viewmodel.CharacterVMFactory
import com.example.bobsburgers.viewmodel.CharacterViewModel

class MainActivity : AppCompatActivity() {

    // Declare binding, vm, vmFactory, and adapter that we'll need
    lateinit var binding: ActivityMainBinding
    private lateinit var vmFactory: CharacterVMFactory
    private val viewModel:CharacterViewModel by viewModels() { vmFactory }
    private val theAdapter: CharacterAdapter by lazy { CharacterAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        vmFactory = CharacterVMFactory(com.example.bobsdata.model.CharacterRepo(this))
        viewModel.getCharacters()
        initViews()
        initObservers()
        setContentView(binding.root)
    }

    private fun initViews() {
        with(binding.rvInitialCharacters) {
            layoutManager = GridLayoutManager(this@MainActivity, 3)
            adapter = theAdapter
        }
    }

    private fun initObservers() {
        viewModel.characters.observe(this) { state ->
            theAdapter.updateCharacterList(state.characters)
        }

        binding.btnAddMore.setOnClickListener {
            with(binding.rvAddMoreCharacters) {
                layoutManager = GridLayoutManager(this@MainActivity, 3)
                adapter = theAdapter
            }
            viewModel.characters.observe(this) { state ->
                theAdapter.updateCharacterList(state.characters)
            }
        }
    }
}