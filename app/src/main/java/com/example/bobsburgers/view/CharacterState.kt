package com.example.bobsburgers.view

import com.example.bobsdata.model.remote.objects.CharacterObjectItem

data class CharacterState(
    val isLoading: Boolean = false,
    val characters: List<com.example.bobsdata.model.remote.objects.CharacterObjectItem> = listOf()

)
