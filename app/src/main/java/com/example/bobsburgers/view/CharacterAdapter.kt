package com.example.bobsburgers.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.viewmodel.viewModelFactory
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.bobsburgers.databinding.CharacterItemBinding
import com.example.bobsdata.model.remote.objects.CharacterObjectItem

class CharacterAdapter(): RecyclerView.Adapter<CharacterAdapter.CharacterViewHolder>() {

    private var charactersList: MutableList<com.example.bobsdata.model.remote.objects.CharacterObjectItem> = mutableListOf()

    class CharacterViewHolder(val binding: CharacterItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val TAG = "Adapter"
        fun displayCharacter(character: com.example.bobsdata.model.remote.objects.CharacterObjectItem) {
                binding.ivCharacterImage.load(character.image)

                binding.tvCharacterName.text = character.name

                binding.btnAddFavorite.setOnClickListener {
                    Log.e(TAG, "Adding to favorites!", )
                    character.favorite = true
                }

                binding.btnDelete.setOnClickListener {
                    Log.e(TAG, "Deleting from view!", )
                    binding.ivCharacterImage.load("")
                    binding.tvCharacterName.text = ""


                }
            }
        }

    fun updateCharacterList(newCharactersList: List<com.example.bobsdata.model.remote.objects.CharacterObjectItem>) {
        val oldSize = charactersList.size
        charactersList.clear()
        notifyItemRangeChanged(0, oldSize)
        charactersList.addAll(newCharactersList)
        notifyItemRangeChanged(0, newCharactersList.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterAdapter.CharacterViewHolder {
        return CharacterAdapter.CharacterViewHolder(
            CharacterItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {
        val character = charactersList[position]
        holder.displayCharacter(character)
    }

    override fun getItemCount(): Int = charactersList.size
}