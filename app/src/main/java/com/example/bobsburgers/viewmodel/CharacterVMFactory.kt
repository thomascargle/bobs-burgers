package com.example.bobsburgers.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.bobsdata.model.CharacterRepo

class CharacterVMFactory(
    private val repo: com.example.bobsdata.model.CharacterRepo
): ViewModelProvider.NewInstanceFactory() {

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return CharacterViewModel(repo) as T
    }
}