package com.example.bobsburgers.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.bobsdata.model.CharacterRepo
import com.example.bobsdata.model.local.CharacterDao
import com.example.bobsdata.model.remote.objects.CharacterObjectItem
import com.example.bobsburgers.view.CharacterState
import kotlinx.coroutines.launch

// Bringing in the repo to the VM is a version of DependencyInjection
class CharacterViewModel(private val repo: com.example.bobsdata.model.CharacterRepo) : ViewModel() {

    private val _characters: MutableLiveData<CharacterState> = MutableLiveData(CharacterState())
    val characters: LiveData<CharacterState> get() = _characters

    fun getCharacters() {
        val job = viewModelScope.launch {
            _characters.value = CharacterState(
                characters = repo.getCharacters() as List<com.example.bobsdata.model.remote.objects.CharacterObjectItem>
            )
        }
    }

    /*fun deleteCharacters() {
        val job = viewModelScope.launch {
            _characters.value =
        }
    }*/
}