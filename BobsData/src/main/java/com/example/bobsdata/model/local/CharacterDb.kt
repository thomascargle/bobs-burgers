package com.example.bobsdata.model.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

// Get the entities to use in the database
@Database(entities = [CharacterEntity::class], version = 1)
abstract class CharacterDb: RoomDatabase() {

    abstract fun characterDao(): CharacterDao

    companion object {
        private const val DATABASE_NAME = "Character.db"

        // We don't want to cache this since it will be changing
        @Volatile
        // Declare an instance of the DB, but make it nullable since we won't have this until we get the instance
        private var instance: CharacterDb? = null

        fun getInstance(context: Context): CharacterDb {
            return instance ?: synchronized(this) { // Do something in case instance is null
                // Maybe by the time we get here, it's not null. Check, then default to building the db
                // Remember this will return an error until we create function to build the DB (below)
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        // Build our DB, return room. Pass context, the db we created, and the db name. Don't forget to build()
        private fun buildDatabase(context: Context): CharacterDb {
            return Room
                .databaseBuilder(context, CharacterDb::class.java, DATABASE_NAME)
                .build()
        }
    }
}