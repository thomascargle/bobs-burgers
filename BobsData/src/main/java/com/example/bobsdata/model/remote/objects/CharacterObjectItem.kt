package com.example.bobsdata.model.remote.objects

data class CharacterObjectItem(
    val age: String = "",
    val firstEpisode: String = "",
    val gender: String = "",
    val hairColor: String = "",
    val id: Int = 0,
    val image: String = "",
    val name: String = "",
    val occupation: String = "",
    val relatives: List<Relative> = listOf(),
    val url: String = "",
    val voicedBy: String = "",
    val wikiUrl: String = "",
    var favorite: Boolean = false
)