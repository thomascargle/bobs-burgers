package com.example.bobsdata.model

import android.content.Context
import android.util.Log
import com.example.bobsdata.model.local.CharacterDao
import com.example.bobsdata.model.local.CharacterDb
import com.example.bobsdata.model.local.CharacterEntity
import com.example.bobsdata.model.remote.CharacterService
import com.example.bobsdata.model.remote.objects.CharacterObjectItem
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CharacterRepo(context: Context) {
    private val characterService = CharacterService.getInstance()
    private val characterDao = CharacterDb.getInstance(context).characterDao()

    val TAG = "Repo"
    suspend fun getCharacters() = withContext(Dispatchers.IO) {
        val cachedCharacters = characterDao.getAllCharacters()
        return@withContext characterService.getCharacters().ifEmpty {
            // go to network and get characters
            val remoteCharacters = characterService.getCharacters()

            // Map strings to Character entity
            val entities: List<CharacterEntity> = remoteCharacters.map {
                CharacterEntity(name = it.name, image = it.image)
            }

            // Save the characters
            characterDao.insertCharacter(*entities.toTypedArray())
            return@ifEmpty entities
        }
    }

    /*suspend fun deleteCharacter() = withContext(Dispatchers.IO) {
        return@withContext characterDao.deleteCharacter()
    }*/
}