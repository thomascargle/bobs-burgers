package com.example.bobsdata.model.local

import androidx.room.*

@Dao
interface CharacterDao {
    // Functions with no body go here

    @Query("SELECT * FROM CharacterEntity")
    suspend fun getAllCharacters(): List<CharacterEntity>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCharacter(vararg characterEntity: CharacterEntity)

    @Delete
    suspend fun deleteCharacter(character: CharacterEntity): Boolean = true

}