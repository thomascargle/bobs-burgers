package com.example.bobsdata.model.local

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class CharacterEntity(
    // Include attributes for each thing we could get back from the API

    // PrimaryKey annotation
    @PrimaryKey(autoGenerate = true)
    val id: Int = 0,
    val name: String = "",
    val image: String = "",
    val gender: String = "",
    val hairColor: String = "",
    val occupation: String = "",
    val firstEpisode: String = "",
    val voicedBy: String = "",
    val url: String = "",
    val wikiUrl: String = "",
    var favorite: Boolean = false
)

    // The attributes we get back from the API, with examples
    /*"id": 1,
    "name": "\"Dottie Minerva\"",
    "image": "https://bobsburgers-api.herokuapp.com/images/characters/1.jpg",
    "gender": "Female",
    "hairColor": "Blonde",
    "occupation": "Student at Wagstaff School",
    "firstEpisode": "\"The Kids Run the Restaurant\"",
    "voicedBy": "Wendy Molyneux",
    "url": "https://bobsburgers-api.herokuapp.com/characters/1",
    "wikiUrl": "https://bobs-burgers.fandom.com/wiki/%22Dottie_Minerva%22"*/