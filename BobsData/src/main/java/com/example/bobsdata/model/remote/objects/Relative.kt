package com.example.bobsdata.model.remote.objects

data class Relative(
    val name: String,
    val relationship: String,
    val wikiUrl: String
)