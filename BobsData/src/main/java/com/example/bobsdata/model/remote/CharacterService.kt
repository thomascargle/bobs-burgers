package com.example.bobsdata.model.remote

import com.example.bobsdata.model.remote.objects.CharacterObject
import com.example.bobsdata.model.remote.objects.CharacterObjectItem
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create
import retrofit2.http.GET
import retrofit2.http.Query

// Services will be interfaces
interface CharacterService {

    companion object {
        private const val BASE_URL = "https://bobsburgers-api.herokuapp.com"
        private const val CHARACTERS_ENDPOINT = "/characters"

        // Get an instance of the service
        fun getInstance(): CharacterService = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
            .create()
    }

    @GET(CHARACTERS_ENDPOINT)
    suspend fun getCharacters(@Query("limit") limit: Int = 6): List<CharacterObjectItem>

    @GET(CHARACTERS_ENDPOINT)
    suspend fun addCharacters(@Query("limit") limit: Int = 6): List<CharacterObjectItem>
}